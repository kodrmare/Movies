package com.example.marek.movies.viewmodel;

import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.fragment.GaleryFragment;
import com.example.marek.movies.view.GaleryView;

import org.alfonz.view.StatefulLayout;


public class GaleryViewModel extends BaseViewModel<GaleryView>
{
	public final ObservableField<Integer> state = new ObservableField<>();
	public final ObservableField<MessageEntity> message = new ObservableField<>();
	private int mPosition;


	@Override
	public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState)
	{

		handleArguments(arguments);

	}



	@Override
	public void onStart()
	{
		super.onStart();
		if(message.get() == null) loadData();


	}


	private void loadData()
	{
		// show progress
		state.set(StatefulLayout.PROGRESS);

		// load data from data provider...
		onLoadData();
	}


	private void onLoadData()
	{
		// save data
		message.set(new MessageEntity("Hello World " + mPosition));

		// show content
		if(message.get() != null)
		{
			state.set(StatefulLayout.CONTENT);
		}
		else
		{
			state.set(StatefulLayout.EMPTY);
		}
	}

	private void handleArguments(Bundle arguments){
		if(arguments != null){
			mPosition = arguments.getInt(GaleryFragment.ARGUMENT_POSITION);
		}
	}
}
