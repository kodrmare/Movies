package com.example.marek.movies.viewmodel;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;

import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.view.ListingView;

import org.alfonz.view.StatefulLayout;


public class ListingViewModel extends BaseViewModel<ListingView>
{
	public final ObservableField<Integer> state = new ObservableField<>();
	public final ObservableArrayList<MessageEntity> messages = new ObservableArrayList<>();
	public final ObservableArrayList<String> titles = new ObservableArrayList<>();


	@Override
	public void onStart()
	{
		super.onStart();
		if(messages.isEmpty()) loadData();
	}


	private void loadData()
	{
		// show progress
		state.set(StatefulLayout.PROGRESS);

		// load data from data provider...
		onLoadData();
	}


	private void onLoadData()
	{
		// save data
		for(int i = 0; i < 32; i++)
		{
			messages.add(new MessageEntity("Item " + i));
		}

		for(int i = 0; i < 4; i++)
		{
			titles.add(new String("Title " + i));
		}

		// show content
		if(!messages.isEmpty())
		{
			state.set(StatefulLayout.CONTENT);
		}
		else
		{
			state.set(StatefulLayout.EMPTY);
		}
	}
}
