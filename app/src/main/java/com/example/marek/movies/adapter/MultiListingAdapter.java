package com.example.marek.movies.adapter;

import com.example.marek.movies.R;
import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.view.ListingView;
import com.example.marek.movies.viewmodel.ListingViewModel;

import org.alfonz.adapter.MultiDataBoundRecyclerAdapter;


public class MultiListingAdapter extends MultiDataBoundRecyclerAdapter
{
	public MultiListingAdapter(ListingView view, ListingViewModel viewModel)
	{
		super(view, viewModel.messages, viewModel.titles);
	}


	@Override
	public int getItemLayoutId(int position)
	{
		Object item = getItem(position);
		if(item instanceof String)
		{
			return R.layout.fragment_listing_item_title;
		}
		else if(item instanceof MessageEntity)
		{
			return R.layout.fragment_listing_item;
		}
		throw new IllegalArgumentException("Unknown item type " + item);
	}
}
