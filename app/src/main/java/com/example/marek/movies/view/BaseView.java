package com.example.marek.movies.view;

import org.alfonz.mvvm.AlfonzView;


public interface BaseView extends AlfonzView
{
	void showToast(String message);
}
