package com.example.marek.movies.fragment;

import android.databinding.ViewDataBinding;
import android.widget.Toast;

import com.example.marek.movies.activity.BaseActivity;
import com.example.marek.movies.view.BaseView;
import com.example.marek.movies.viewmodel.BaseViewModel;

import org.alfonz.mvvm.AlfonzBindingFragment;


public abstract class BaseFragment<T extends BaseView, R extends BaseViewModel<T>, B extends ViewDataBinding>
		extends AlfonzBindingFragment<T, R, B> implements BaseView
{
	@Override
	public void showToast(String message)
	{
		((BaseActivity)getActivity()).showToastMessage(message);
	}
}
