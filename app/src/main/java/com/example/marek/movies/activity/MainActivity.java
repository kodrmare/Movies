package com.example.marek.movies.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.marek.movies.R;

import static org.alfonz.mvvm.AlfonzActivity.INDICATOR_NONE;


public class MainActivity extends BaseActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setupActionBar(INDICATOR_NONE);
	}
}
