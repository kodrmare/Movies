package com.example.marek.movies.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marek.movies.R;
import com.example.marek.movies.activity.GaleryActivity;
import com.example.marek.movies.adapter.GaleryAdapter;
import com.example.marek.movies.databinding.FragmentGaleryBinding;
import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.view.GaleryView;
import com.example.marek.movies.viewmodel.GaleryViewModel;


public class GaleryFragment
		extends BaseFragment<GaleryView, GaleryViewModel, FragmentGaleryBinding>
		implements GaleryView
{
	public static String ARGUMENT_POSITION = "position";

	private GaleryAdapter mAdapter;

	public static Fragment newInstance(int position){

		Bundle bundle = new Bundle();
		bundle.putInt(ARGUMENT_POSITION, position);

		Fragment fragment = new GaleryFragment();
		fragment.setArguments(bundle);
		return fragment;
	}

	@Nullable
	@Override
	public Class<GaleryViewModel> getViewModelClass()
	{
		return GaleryViewModel.class;
	}


	@Override
	public FragmentGaleryBinding inflateBindingLayout(LayoutInflater inflater)
	{
		return FragmentGaleryBinding.inflate(inflater);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}


	@Override
	public void onClick(MessageEntity message)
	{
		showToast(message.getMessage());
	}


}