package com.example.marek.movies.activity;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marek.movies.R;

import org.alfonz.mvvm.AlfonzActivity;


public abstract class BaseActivity extends AlfonzActivity
{
	// add whatever you need...

	public void showToastMessage(String textToast){
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast, (ViewGroup) findViewById(R.id.custom_toast_container));

		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(textToast);

		Toast toast = new Toast(getApplicationContext());
		toast.setGravity(Gravity.BOTTOM, 0, 150);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}
}
