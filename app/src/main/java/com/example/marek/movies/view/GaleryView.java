package com.example.marek.movies.view;

import com.example.marek.movies.entity.MessageEntity;


public interface GaleryView extends BaseView
{
	void onClick(MessageEntity message);
}
