package com.example.marek.movies.entity;

public class MovieEntity
{

	private String movie;
	private String description;
	private String URL;


	public MovieEntity(String movie, String description, String URL)
	{
		this.movie = movie;
		this.description = description;
		this.URL = URL;
	}


	public String getMovie()
	{
		return movie;
	}


	public void setMovie(String movie)
	{
		this.movie = movie;
	}


	public String getDescription()
	{
		return description;
	}


	public void setDescription(String description)
	{
		this.description = description;
	}


	public String getURL()
	{
		return URL;
	}


	public void setURL(String URL)
	{
		this.URL = URL;
	}


}
