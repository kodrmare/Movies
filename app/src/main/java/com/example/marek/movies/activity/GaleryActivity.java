package com.example.marek.movies.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.marek.movies.R;
import com.example.marek.movies.adapter.GaleryAdapter;


public class GaleryActivity extends BaseActivity
{
	public static Intent newIntent(Context context)
	{
		return new Intent(context, GaleryActivity.class);
	}

	private GaleryAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_galery);
		setupActionBar(INDICATOR_NONE);
		setupAdapter();
		setBottomBar();


		//tohle je fakt spatny
		/*
		TabLayout tabs = (TabLayout) findViewById(R.id.tabs);

		tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab)
			{

				if(tab.getPosition() == 0){
					tab.setText("POPULAR");
				} else{
					tab.setText("NOW PLAYING");

				}
			}


			@Override
			public void onTabUnselected(TabLayout.Tab tab)
			{

			}


			@Override
			public void onTabReselected(TabLayout.Tab tab)
			{

			}
		});
		*/
	}

	private void setupAdapter()
	{
		if(mAdapter == null)
		{
			mAdapter = new GaleryAdapter(getSupportFragmentManager());
			ViewPager viewPager = (ViewPager) findViewById(R.id.activity_galery_viewpager);
			viewPager.setAdapter(mAdapter);
		}
	}

	public void setActionBarTitle(String title){
		setupActionBar(INDICATOR_NONE, title);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	//search button clicked
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if(id == R.id.action_search){
			Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void setBottomBar(){
		BottomNavigationView mBottomNav = (BottomNavigationView) findViewById(R.id.navigation);

		//handling bottomNavigationBar
		mBottomNav.setOnNavigationItemSelectedListener(

				new BottomNavigationView.OnNavigationItemSelectedListener() {
					@Override
					public boolean onNavigationItemSelected(@NonNull MenuItem item) {
						Fragment fragment = null;

						if(item.getTitle().equals("Movies") ){
							setTitle("Movies");
							showToastMessage("Movies");
							//fragment = new ListFragment();
						}
						else if(item.getTitle().equals("Actors")){
							setTitle("Actors");
							showToastMessage("Actors");
							//fragment = new ActorsFragment();
						}
						else if(item.getTitle().equals("Profile")){
							setTitle("");
							showToastMessage("Profile");
							//fragment = new ProfileFragment();
						}
						/*
						FragmentManager fragmentManager = getSupportFragmentManager();
						android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
						fragmentTransaction.replace(R.id.root_layout, fragment);
						fragmentTransaction.commit();
						*/
						return true;
					}
				});
	}
}

