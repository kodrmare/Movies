package com.example.marek.movies.adapter;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.TableLayout;

import com.example.marek.movies.activity.BaseActivity;
import com.example.marek.movies.fragment.BaseFragment;
import com.example.marek.movies.fragment.GaleryFragment;
import com.example.marek.movies.fragment.ListingFragment;

import eu.inloop.viewmodel.support.ViewModelStatePagerAdapter;


public class GaleryAdapter extends ViewModelStatePagerAdapter
{
	private static final int FRAGMENT_COUNT = 2;


	public GaleryAdapter(FragmentManager fragmentManager)
	{
		super(fragmentManager);
	}


	public static String getFragmentTag(int viewPagerId, int position)
	{
		return "android:switcher:" + viewPagerId + ":" + position;
	}


	@Override
	public int getCount()
	{
		return FRAGMENT_COUNT;
	}

	@Override
	public Fragment getItem(int position)
	{
		if(position == 0) {
			return new ListingFragment();
		}
		else
		{
			//return new GaleryFragment();
			return GaleryFragment.newInstance(position);
		}
	}


	@Override
	public CharSequence getPageTitle(int position)
	{
		return "Fragment " + position;
	}


	public void refill()
	{
		notifyDataSetChanged();
	}
}