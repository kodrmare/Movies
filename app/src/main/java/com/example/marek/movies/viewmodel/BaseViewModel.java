package com.example.marek.movies.viewmodel;

import com.example.marek.movies.view.BaseView;

import org.alfonz.mvvm.AlfonzViewModel;


public abstract class BaseViewModel<T extends BaseView> extends AlfonzViewModel<T>
{
	public void handleError(String message)
	{
		if(getView() != null)
		{
			getView().showToast(message);
		}
	}
}