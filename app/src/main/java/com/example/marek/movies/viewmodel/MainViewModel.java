package com.example.marek.movies.viewmodel;

import android.databinding.ObservableField;

import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.entity.MovieEntity;
import com.example.marek.movies.view.MainView;

import org.alfonz.view.StatefulLayout;


public class MainViewModel extends BaseViewModel<MainView>
{
	public final ObservableField<Integer> state = new ObservableField<>();
	public final ObservableField<MessageEntity> message = new ObservableField<>();


	@Override
	public void onStart()
	{
		super.onStart();
		if(message.get() == null) loadData();
	}


	public void updateMessage(String text)
	{
		MessageEntity m = message.get();
		m.setMessage(text);
		message.notifyChange();
	}


	private void loadData()
	{
		// show progress
		state.set(StatefulLayout.PROGRESS);

		// load data from data provider...
		onLoadData(new MessageEntity("HELLO"));
	}


	private void onLoadData(MessageEntity m)
	{
		// save data
		message.set(m);

		// show content
		if(message.get() != null)
		{
			state.set(StatefulLayout.CONTENT);
		}
		else
		{
			state.set(StatefulLayout.EMPTY);
		}
	}
}