package com.example.marek.movies.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.marek.movies.R;


public class ListingActivity extends BaseActivity
{
	public static Intent getIntent(Context context)
	{
		return new Intent(context, ListingActivity.class);
	}


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listing);
		setupActionBar(INDICATOR_NONE);
	}
}