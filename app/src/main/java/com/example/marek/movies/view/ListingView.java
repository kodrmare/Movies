package com.example.marek.movies.view;

import com.example.marek.movies.entity.MessageEntity;


public interface ListingView extends BaseView
{
	void onClick(MessageEntity message);
}
