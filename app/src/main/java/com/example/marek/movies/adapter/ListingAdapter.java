package com.example.marek.movies.adapter;

import com.example.marek.movies.R;
import com.example.marek.movies.databinding.FragmentListingItemBinding;
import com.example.marek.movies.view.ListingView;
import com.example.marek.movies.viewmodel.ListingViewModel;

import org.alfonz.adapter.SimpleDataBoundRecyclerAdapter;


public class ListingAdapter extends SimpleDataBoundRecyclerAdapter<FragmentListingItemBinding>
{
	public ListingAdapter(ListingView view, ListingViewModel viewModel)
	{
		super(R.layout.fragment_listing_item, view, viewModel.messages);
	}
}
