package com.example.marek.movies.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marek.movies.R;
import com.example.marek.movies.activity.GaleryActivity;
import com.example.marek.movies.activity.ListingActivity;
import com.example.marek.movies.databinding.FragmentMainBinding;
import com.example.marek.movies.view.MainView;
import com.example.marek.movies.viewmodel.MainViewModel;


public class MainFragment
		extends BaseFragment<MainView, MainViewModel, FragmentMainBinding>
		implements MainView
{
	@Nullable
	@Override
	public Class<MainViewModel> getViewModelClass()
	{
		return MainViewModel.class;
	}


	@Override
	public FragmentMainBinding inflateBindingLayout(LayoutInflater inflater)
	{
		return FragmentMainBinding.inflate(inflater);
	}


	@Override
	public void onClick()
	{
//		getViewModel().updateMessage("TIME: " + System.currentTimeMillis());
//		startListingActivity();
		startGaleryActivity();
	}


	private void startListingActivity()
	{
		Intent intent = ListingActivity.getIntent(getActivity());
		startActivity(intent);
	}
	private void startGaleryActivity()
	{
		Intent intent = GaleryActivity.newIntent(getActivity());
		startActivity(intent);
	}
}
