package com.example.marek.movies.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.marek.movies.R;
import com.example.marek.movies.activity.GaleryActivity;
import com.example.marek.movies.activity.ListingActivity;
import com.example.marek.movies.adapter.MultiListingAdapter;
import com.example.marek.movies.databinding.FragmentListingBinding;
import com.example.marek.movies.entity.MessageEntity;
import com.example.marek.movies.view.ListingView;
import com.example.marek.movies.viewmodel.ListingViewModel;


public class ListingFragment
		extends BaseFragment<ListingView, ListingViewModel, FragmentListingBinding>
		implements ListingView
{
	private MultiListingAdapter mAdapter;


	@Nullable
	@Override
	public Class<ListingViewModel> getViewModelClass()
	{
		return ListingViewModel.class;
	}


	@Override
	public FragmentListingBinding inflateBindingLayout(LayoutInflater inflater)
	{
		return FragmentListingBinding.inflate(inflater);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		setupAdapter();
		((GaleryActivity) getActivity()).setActionBarTitle("Movies");
		showToast("cool toast");
	}


	@Override
	public void onClick(MessageEntity message)
	{
		showToast(message.getMessage());
	}


	private void setupAdapter()
	{
		if(mAdapter == null)
		{
			mAdapter = new MultiListingAdapter(this, getViewModel());
			getBinding().fragmentListingRecycler.setAdapter(mAdapter);
		}
	}
}